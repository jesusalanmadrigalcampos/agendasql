package com.example.sqlagenda;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import database.AgendaContactos;
import database.AgendaDbHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private TextView lblRespuesta;
    private Button btnInsertar;
    private TextView lblNombre;
    private Button btnBuscar;
    private EditText txtid;
    private AgendaContactos db;


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
        btnInsertar = (Button) findViewById(R.id.btnInsertar);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        txtid = (EditText) findViewById(R.id.txtNombre);
        db = new AgendaContactos(MainActivity.this);

        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contacto c = new Contacto(1,
                        "Jose Lopez",
                        "6692216927",
                        "6691143572",
                        "Pascual Orozco #501",
                        "Hola",
                        0);
                db.openDataBase();
                long idx = db.insertContacto(c);
                lblRespuesta.setText("Se agrego contacto con id " + idx);

                db.cerrar();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contacto contacto;
                long id = Long.parseLong(txtid.getText().toString());
                db.openDataBase();
                contacto = db.getContacto(id);
                if(contacto == null) {
                    lblNombre.setText("No existe el id");
                }
                else {
                    lblNombre.setText(contacto.getNombre());
                }

                db.cerrar();
            }
        });
    }
}
